package com.lkakulia.lecture_15.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lkakulia.lecture_15.R
import com.lkakulia.lecture_15.models.ItemModel
import kotlinx.android.synthetic.main.item_male_recyclerview_layout.view.*
import kotlinx.android.synthetic.main.item_female_recyclerview_layout.view.*

class RecyclerViewAdapter(private val items: ArrayList<ItemModel>):
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val MALE_ITEMS = 1
        const val FEMALE_ITEMS = 2
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == MALE_ITEMS)
            return MaleViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_male_recyclerview_layout, parent, false))
        else
            return FemaleViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_female_recyclerview_layout, parent, false))
    }



    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is MaleViewHolder)
            holder.onBind()
        else if(holder is FemaleViewHolder)
            holder.onBind()
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class MaleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private lateinit var model: ItemModel

        fun onBind() {
            model = items[adapterPosition]
            itemView.maleImageView.setImageResource(model.image)
            itemView.maleTextView.text = model.title
        }
    }

    inner class FemaleViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        private lateinit var model: ItemModel

        fun onBind() {
            model = items[adapterPosition]
            itemView.femaleImageView.setImageResource(model.image)
            itemView.femaleTextView.text = model.title
        }
    }

    override fun getItemViewType(position: Int): Int {
        val model = items[position]

        if(model.title == "Male Avatar") {
            return MALE_ITEMS
        }

        return FEMALE_ITEMS
    }
}