package com.lkakulia.lecture_15.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.GridLayout
import androidx.recyclerview.widget.GridLayoutManager
import com.lkakulia.lecture_15.R
import com.lkakulia.lecture_15.adapters.RecyclerViewAdapter
import com.lkakulia.lecture_15.models.ItemModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val items = ArrayList<ItemModel>()
    private lateinit var adapter: RecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setUp()
    }

    private fun setUp() {
        recyclerView.layoutManager = GridLayoutManager(this, 2)
        adapter = RecyclerViewAdapter(items)
        recyclerView.adapter = adapter
        setData()

        swipeRefreshLayout.setOnRefreshListener({
            swipeRefreshLayout.isRefreshing = true
            refresh()

            Handler().postDelayed({
                swipeRefreshLayout.isRefreshing = false
                setData()
                adapter.notifyDataSetChanged()
            }, 2000)
        })
    }

    private fun refresh() {
        items.clear()
        adapter.notifyDataSetChanged()
    }


    private fun setData() {
        items.add(ItemModel(R.mipmap.ic_avatar_female, "Female Avatar"))
        items.add(ItemModel(R.mipmap.ic_avatar_male, "Male Avatar"))
        items.add(ItemModel(R.mipmap.ic_avatar_male, "Male Avatar"))
        items.add(ItemModel(R.mipmap.ic_avatar_female, "Female Avatar"))
        items.add(ItemModel(R.mipmap.ic_avatar_female, "Female Avatar"))
        items.add(ItemModel(R.mipmap.ic_avatar_female, "Female Avatar"))
        items.add(ItemModel(R.mipmap.ic_avatar_male, "Male Avatar"))
        items.add(ItemModel(R.mipmap.ic_avatar_male, "Male Avatar"))
        items.add(ItemModel(R.mipmap.ic_avatar_female, "Female Avatar"))
        items.add(ItemModel(R.mipmap.ic_avatar_male, "Male Avatar"))
        items.add(ItemModel(R.mipmap.ic_avatar_female, "Female Avatar"))
        items.add(ItemModel(R.mipmap.ic_avatar_female, "Female Avatar"))
        adapter.notifyDataSetChanged()
    }
}
